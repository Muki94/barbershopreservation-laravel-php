<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    protected $primaryKey = 'id';

    public function myUser()
    {
        return $this->belongsTo('App\User', 'id', 'user_id');
    }
}
