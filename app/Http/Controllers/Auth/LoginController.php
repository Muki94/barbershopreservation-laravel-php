<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::CONTACTS;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('guest')->except('logout');
    // }

    public function index()
    {
        if (Auth::check()) {
            return redirect('contacts');
        }else{
            return view('auth.login');
        }
    }

    public function authenticate(Request $request)
    {
        $credentials = [
            "email" => $request->username,
            "password" => $request->password
        ];

        if (Auth::attempt($credentials)) {
            return redirect('contacts');
        }
    }

    public function loginSecret(){
        return view('auth.secretUserAdd');
    }

    public function registerSecret(Request $request){
        $user = User::create([
            'name'     => $request->name,
            'email'    => $request->username,
            'provider'    => "",
            'provider_id'    => "",
            'isUserAdmin' => 1,
            'password' => Hash::make($request->password),
        ]);

        return redirect('/login');
    }
}
