<?php

namespace App\Http\Controllers;

use App\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class ContactController extends Controller
{
    //get all contacts 
    public function index()
    {
        if(Auth::check()){
            $user = \App\User::find(Auth::user()->id)->first();
            if($user->isUserAdmin == 0)
                return redirect('/');
        }

        $contacts = Contact::orderBy('created_at','desc')->get();
        return view('contacts.index')->with(['contacts' => $contacts]);
    }

    //get single contact
    public function single(int $contactId)
    {
        if(Auth::check()){
            $user = \App\User::find(Auth::user()->id)->first();
            if($user->isUserAdmin == 0)
                return redirect('/');
        }

        $contact = Contact::where('id', $contactId)->first();
        return view('contacts.details')->with(['contact' => $contact]);
    }

    //add new contact 
    public function add(Request $request)
    {
        $contact = new Contact();
        $contact->fname = $request->fname;
        $contact->lname = $request->lname;
        $contact->text = $request->subject;
        $contact->save();

        return redirect('/');
    }

    //delete contact
    public function delete(int $contact)
    {
        if(Auth::check()){
            $user = \App\User::find(Auth::user()->id)->first();
            if($user->isUserAdmin == 0)
                return redirect('/');
        }

        $contactModel = Contact::where('id', $contact)->first();

        $contactModel->forceDelete();

        return redirect('contacts');
    }
}
