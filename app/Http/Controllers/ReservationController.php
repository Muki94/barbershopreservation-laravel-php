<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Reservation;
use App\User;
use Exception;
use Illuminate\Support\Facades\Auth;

class ReservationController extends Controller
{
    //get all reservations 
    function getAll(Request $request)
    {
        // if(Auth::check()){
        //     $user = \App\User::find(Auth::user()->id)->first();
        //     if(!$user->isUserAdmin)
        //         return redirect('/');
        // }

        if ($request->exists("date_of_reservation") && $request->exists("time_of_reservation")) {
            $reservationWithSameDate = Reservation::where('date_of_reservation', '=', $request->date_of_reservation)
                ->where('time_of_reservation', '=', $request->time_of_reservation)
                ->first();

            $reservationAlreadyExists = $reservationWithSameDate != null ? true : false;

            if ($reservationAlreadyExists) {
                $allReservedDates = Reservation::select('date_of_reservation', 'time_of_reservation')
                    ->distinct()
                    ->get();

                return response()->json([
                    'code' => 200,
                    'status' => 'TAKEN',
                    'data' => $allReservedDates,
                    'message' => 'Termin koji ste odabrali je nažalost zauzet!'
                ]);
            }
        }

        return response()->json([
            'code' => 200,
            'status' => 'SUCCESS',
            'data' => Reservation::where('user_id', $request->userId)->get(),
            'message' => ''
        ]);
    }

    function index()
    {
        if (Auth::check()) {
            $user = \App\User::find(Auth::user()->id)->first();
            if ($user->isUserAdmin == 0)
                return redirect('/');
        }

        $reservations = Reservation::orderBy('date_of_reservation')->orderBy('time_of_reservation')->get();
        return view('reservations.index')->with(['reservations' => $reservations]);
    }

    //add new reservation 
    function add(Request $request)
    {
        try {
            $reservation = new Reservation();
            $loggedUser = User::where('id', Auth::user()->id)->first();

            if ($loggedUser->reservation_cancelation > 3) {
                return response()->json([
                    'code' => 404,
                    'status' => 'ERROR',
                    'message' => 'Otkazali ste rezervaciju tri puta te Vam više nije dozvoljeno pravljenje rezervacija!'
                ]);
            }

            $reservation->reservation_type = $request->reservation_type;
            $reservation->date_of_reservation = $request->date_of_reservation;
            $reservation->time_of_reservation = $request->time_of_reservation;
            $reservation->user_id = Auth::user()->id;
            $reservation->save();

            return response()->json([
                'code' => 200,
                'status' => 'SUCCESS',
                'message' => 'Uspješno ste napravili rezervaciju, istu možete poništiti ali Vam se piše kazneni bod.(3 boda i nije Vam moguće praviti rezervacije.)'
            ]);
        } catch (Exception $ex) {
            return response()->json([
                'code' => 404,
                'status' => 'ERROR',
                'message' => 'Došlo je do greške prilikom dodavanja rezervacije, javite se Administratoru! \n' + $ex->getMessage()
            ]);
        }
    }

    //delete reservation
    function delete(int $id)
    {
        try {
            $reservation = Reservation::where('id', $id)->first();
            $loggedUser = User::where('id', Auth::user()->id)->first();

            $reservation->forceDelete();

            $loggedUser->reservation_cancelation = $loggedUser->reservation_cancelation + 1;
            $loggedUser->save();

            return response()->json([
                'code' => 200,
                'status' => 'SUCCESS',
                'message' => 'Uspješno ste poništili rezervaciju, ukupno imate ' . $loggedUser->reservation_cancelation . ' bod/a.(3 boda i nije Vam moguće praviti rezervacije.)'
            ]);
        } catch (Exception $ex) {
            return response()->json([
                'code' => 404,
                'status' => 'ERROR',
                'message' => 'Došlo je do greške prilikom brisanja rezervacije, javite se Administratoru! \n' + $ex->getMessage()
            ]);
        }
    }
}
