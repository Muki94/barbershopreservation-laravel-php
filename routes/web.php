<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

//login routes
Route::get('/login', 'Auth\LoginController@index')->name('login');
Route::post('/auth', 'Auth\LoginController@authenticate')->name('authenticate');
Route::get('/loginSecret', 'Auth\LoginController@loginSecret')->name('loginSecret');
Route::post('/registerSecret', 'Auth\LoginController@registerSecret')->name('registerSecret');
Route::post('/logout', 'Auth\LoginController@logout')->name('logout');
// Auth::routes();

//contacts routes
Route::get('/contacts', 'ContactController@index')->name('contacts')->middleware('auth');
Route::get('/contact/{id}', 'ContactController@single')->name('contact')->middleware('auth');
Route::post('/contacts/add', 'ContactController@add')->name('contactAdd');
Route::get('/contacts/delete/{id}', 'ContactController@delete')->name('contactDelete')->middleware('auth');

//reservation routes
Route::get('/api/reservations', 'ReservationController@getAll')->name('reservationsApi')->middleware('auth');
Route::get('/reservations', 'ReservationController@index')->name('reservations')->middleware('auth');
Route::post('/reservations/add', 'ReservationController@add')->name('reservationAdd')->middleware('auth');
Route::get('/reservations/delete/{id}', 'ReservationController@delete')->name('reservationDelete')->middleware('auth');

//social login auth routes
Route::get('/auth/redirect/{provider}', 'SocialController@redirect');
Route::get('/callback/{provider}', 'SocialController@callback');
