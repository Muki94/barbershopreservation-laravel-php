@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12" style="overflow: auto">
            <table class="table">
                <thead>
                    <th style="width:15%;">
                        Ime
                    </th>
                    <th style="width:15%;">
                        Prezime
                    </th>
                    <th style="width:30%;">
                        Text
                    </th>
                    <th style="width:20%;">
                        Napravljeno
                    </th>
                    <th style="width:20%;">

                    </th>
                </thead>
                <tbody>
                    @foreach ($contacts as $contact)
                    <tr>
                        <td>{{ $contact-> fname }}</td>
                        <td>{{ $contact-> lname }}</td>
                        <td>{{ $contact-> text }}</td>
                        <td>{{ $contact-> created_at }}</td>
                        <td class="row">
                            <a href="/contact/{{$contact->id}}" style="margin-right: 10px">Detalji</a> |
                            <a href="/contacts/delete/{{$contact->id}}" style="margin-left: 10px">Obriši</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection