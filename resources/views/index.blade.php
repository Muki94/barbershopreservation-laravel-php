<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="keywords" content="barber,ensar secic, frizer, frizer zepce, rezervacija, rezervacija frizera">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

	<title>Booking Form HTML Template</title>

	<!-- Google font -->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">

	<!-- Custom stlylesheet -->
	<link type="text/css" rel="stylesheet" href="{{ asset('css/style.css')}}" />
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
	<link rel="stylesheet" href="{{ asset('css/contact-form.css')  }}">
	<link rel="stylesheet" href="{{ asset('css/landing-page.css') }}">
	<link rel="stylesheet" href="{{ asset('css/app.css') }}">
	<link rel="stylesheet" href="{{ asset('css/media-query-landing-page.css') }}">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
</head>

<body data-gr-c-s-loaded="true">
	<div>
	@auth
	<nav class="navbar navbar-expand-md navbar-light shadow-sm">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
     
                        @auth
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                        document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endauth
                    </ul>
                </div>
	        </nav>
			@endauth
		<!-- header section -->
		<section id="booking" class="section">
			<div class="container">
				<div class="section-center">
					<div class="row" style="margin: 0;">
						<div class="col-md-5" style="margin:0 auto;">
							<div class="booking-cta">
								<h1>Rezervišite svoj tretman</h1>
								<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Animi facere, soluta magnam
									consectetur molestias itaque
									ad sint fugit architecto incidunt iste culpa perspiciatis possimus voluptates
									aliquid consequuntur cumque quasi.
									Perspiciatis.
								</p>
							</div>
						</div>
						<div class="col-md-4" style="margin:0 auto;">
							<div class="booking-form">
								<form id="reservation_form">
									@csrf
									<h4 id="error_text" style="color:red;display:none;">Potrebno je unijeti sva polja!</h4>
									<div class="form-group">
										<span class="form-label">Vrsta tretmana</span>
										<input id="treatmant_type" name="reservation_type" class="form-control" type="text" placeholder="Unesite tretman">
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="form-group">
												<label class="form-label" for="flatpickr">Datum rezervacije</label>
												<input id="date_of_reservation" name="date_of_reservation" class="form-control move-down" placeholder="Datum rezervacije...">
											</div>

											<div class="form-group">
												<span class="form-label">Vrijeme rezervacije</span>
												<input id="time_of_reservation" name="time_of_reservation" class="form-control" placeholder="Vrijeme rezervacije...">
											</div>
										</div>

									</div>

									<div class="form-btn">
										<div class="row" style="margin:0;">
											<input type="button" onclick="validateFields();" class="submit-btn mb-4" value="Provjeri dostupnost" />
										</div>
										@auth
										<div class="row" style="margin:0;">
											<a onclick="checkMyReservations();">Provjeri moje rezervacije</a>
										</div>
										@endauth
									</div>

									<script>
										function validateFields() {
											let treatmantType = $("#treatmant_type");
											let dateOfReservation = $("#date_of_reservation");
											let timeOfReservation = $("#time_of_reservation");

											let isValid = true;

											if (!treatmantType.val()) {
												isValid = false;
												treatmantType.addClass("validation-error");
											} else
												treatmantType.removeClass("validation-error");

											if (!dateOfReservation.val()) {
												isValid = false;
												dateOfReservation.addClass("validation-error");
											} else
												dateOfReservation.removeClass("validation-error");

											if (!timeOfReservation.val()) {
												isValid = false;
												timeOfReservation.addClass("validation-error");
											} else
												timeOfReservation.removeClass("validation-error");

											if (isValid) {
												@auth

												function addNewReservation() {
													return new Promise((resolve, reject) => {
														$.ajax({
															url: "/reservations/add",
															type: "POST",
															data: $("#reservation_form").serialize(),
															success: (data) => {
																resolve(data);
															},
															error: (error) => {
																reject(error);
															}
														});
													});
												}

												$.ajax({
													url: `/api/reservations?date_of_reservation=${$("#date_of_reservation").val()}&time_of_reservation=${$("#time_of_reservation").val()}`,
													type: "GET",
													success: (data) => {
														if (data.code == 200 && data.status == "SUCCESS") {
															addNewReservation().then(response => {
																if (response.code == 200 && response.status == "SUCCESS")
																	toastr.success(response.message)
																else
																	toastr.error(response.message);
															}).catch(err => {
																console.log(err);
																toastr.error(err.message)
															});
														} else if (data.code == 200 && data.status == "TAKEN") {
															toastr.info(data.message);
														} else
															toastr.error(data.message)
													},
													error: (error) => {
														console.log(error);
														toastr.error(error.message)
													}
												})
												@else
												$("#loginModal").modal();
												@endauth

												document.getElementById("error_text").style = "display:none;";
											} else
												document.getElementById("error_text").style = "display:block;color:red;text-align:center;margin-bottom:35px";
										}

										@auth

										function checkMyReservations() {
											$("#myReservationsModal").modal();

											$.ajax({
												url: `/api/reservations?userId={{Auth::user()->id}}`,
												type: "GET",
												success: (data) => {
													$("#data_container").html(`
														<div style="overflow-x:auto;">
															<table class='table'>
																<thead>
																	<th>Vrsta tretmana</th>
																	<th>Datum rezervacije</th>
																	<th>Vrijeme rezervacije</th>
																	<th></th>
																</thead>
																<tbody id='table_data'>
																	
																</tbody>
															</table>
														</div>`);

													let reservations = "";

													for (let i = 0; i < data.data.length; i++) {
														reservations += `<tr>
															<td>${data.data[i].reservation_type}</td>
															<td>${data.data[i].date_of_reservation}</td>
															<td>${data.data[i].time_of_reservation}</td>
															<td>
																<button onclick="deleteReservation(this);" class='btn btn-danger' data-reservationId='${data.data[i].id}'>Otkaži</button>
															</td>
															</tr>`;
													}

													$("#table_data").html(reservations);
												},
												error: (error) => {
													console.log(error);
												}
											});
										}

										function deleteReservation(e) {
											let reservation_id = $(e).attr("data-reservationId");
											let current_row = $(e).parent().parent()[0];

											console.log(current_row);

											if (reservation_id && reservation_id > 0) {
												$.ajax({
													url: `/reservations/delete/${reservation_id}`,
													type: "GET",
													success: (data) => {
														if (data.code == 200 && data.status == "SUCCESS") {
															toastr.success(data.message);
															$(current_row).remove();
															console.log(data);
															return;
														}

														toastr.error(data.message);
													},
													error: (err) => {
														console.log(err);
													}
												});
											} else {
												toastr.error("Nešto nije uredu sa rezervacijom, molimo Vas kontaktirajte Administratora!");
											}
										}
										@endauth
									</script>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<!-- about -->
		<section class="section about-section" style="height:auto;">
			<div class="container" style="width:100%">

				<div class="row mb-5 show-on-scroll animate" style="width: 100%; background-color:white; min-height: 100px; padding: 40px 30px;box-shadow: 0px 0px 15px 2px rgba(0,0,0,0.5);border-radius: 10px;">
					<div class="col-md-3 img-mobile-align">
						<img src="{{asset('img/ensarimg/land1.jpg')}}" width="200" height="200" style="border-radius: 50%;">
					</div>
					<div class="col-md-9">
						<article>

							<h4>O nama</h4>

							<p>
								Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a
								piece of
								Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a
								piece of
								Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a
								piece of
								classical Latin literature from 45 BC, making it over 2000 years old. Richard
								McClintock, a Latin
								professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin
								words,
								consectetur, from a Lorem Ipsum passage, and going through the cites of the word in
								classical
								literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32
								and 1.10.33
							</p>
						</article>

					</div>
				</div>

				<div class="row mb-5 show-on-scroll animate" style="width: 100%; background-color:white; min-height: 100px; padding: 40px 30px;box-shadow: 0px 0px 15px 2px rgba(0,0,0,0.5);border-radius: 10px;">
					<div class="col-md-3 img-mobile-align">
						<img src="{{asset('img/ensarimg/land4.jpg')}}" width="200" height="200" style="border-radius: 50%;">
					</div>
					<div class="col-md-9">
						<article>

							<h4>Naši ciljevi</h4>
							<p>
								Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a
								piece of
								Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a
								piece of
								Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a
								piece of
								classical Latin literature from 45 BC, making it over 2000 years old. Richard
								McClintock, a Latin
								professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin
								words,
								consectetur, from a Lorem Ipsum passage, and going through the cites of the word in
								classical
								literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32
								and 1.10.33
							</p>
						</article>

					</div>
				</div>

				<div class="row show-on-scroll animate" style="width: 100%; background-color:white; min-height: 100px; padding: 40px 30px;box-shadow: 0px 0px 15px 2px rgba(0,0,0,0.5);border-radius: 10px;">
					<div class="col-md-3 img-mobile-align">
						<img src="{{asset('img/ensarimg/land6.jpg')}}" width="200" height="200" style="border-radius: 50%;">
					</div>
					<div class="col-md-9">
						<article>

							<h4>Mi brinemo o vašem izgledu</h4>
							<p>
								Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a
								piece of
								Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a
								piece of
								Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a
								piece of
								classical Latin literature from 45 BC, making it over 2000 years old. Richard
								McClintock, a Latin
								professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin
								words,
								consectetur, from a Lorem Ipsum passage, and going through the cites of the word in
								classical
								literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32
								and 1.10.33
							</p>
						</article>

					</div>
				</div>
			</div>
		</section>

		<!-- contact -->
		<section id="contactUs" class="section contact-section" style="position: relative;">
			<div class="container contact-form-design">
				<div style="text-align:center">
					<h2><strong>Kontaktirajte nas</strong></h2>
				</div>
				<div class="row" style="margin:0;">
					<form action="/contacts/add" method="post" style="width:100%;">
						@csrf
						<label for="fname">Ime</label>
						<input type="text" id="fname" name="fname" placeholder="Ime.." class="form-control move-down contact-form-input">
						<label for="lname">Prezime</label>
						<input type="text" id="lname" name="lname" placeholder="Prezime.." class="form-control move-down contact-form-input">
						<label for="subject">Tekst</label>
						<textarea id="subject" name="subject" placeholder="Tekst.." style="height:170px" class="form-control move-down contact-form-input"></textarea>
						<input type="submit" value="Pošalji" class="btn btn-primary contact-form-submit">
					</form>
				</div>
			</div>
		</section>

		<!-- modal section -->
		<section>
			<!-- Modal -->
			<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-modal="true">
				<div class="modal-dialog modal-dialog-centered" role="document">
					<div class="modal-content">
						<div class="modal-body">
							<div style="text-align:center;">
								<h3>Prijava</h3>
								<p class="mb-4">Prije provjere se morate logovati na neku socijalnu mrežu!</p>
							</div>
							<div class="flex-container" style="width:100%; text-align:center;">
								<div class="flex-item facebook-btn social-btn" onclick="logInWithProvider('facebook');">
									<img style="float: left;" src="{{asset('img/svg/facebook-f-brands.svg')}}" alt="Facebook icon" width="50" height="50">
								</div>
								<div class="flex-item google-btn social-btn" onclick="logInWithProvider('google');">
									<img style="float: left;" src="{{asset('img/svg/google-brands.svg')}}" alt="Google icon" width="50" height="50">
								</div>
								<div class="flex-item twiter-btn social-btn" onclick="logInWithProvider('twiter');">
									<img style="float: left;" src="{{asset('img/svg/twitter-brands.svg')}}" alt="Twiter icon" width="50" height="50">
								</div>


								<script>
									function logInWithProvider(provider) {
										let url = "{{ url('/auth/redirect/') }}";
										location.href = url + "/" + provider;
									}
								</script>

							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- Modal -->
			<div class="modal fade" id="myReservationsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-modal="true">
				<div class="modal-dialog modal-dialog-centered" role="document">
					<div class="modal-content">
						<div class="modal-body">
							<div id="data_container" style="width:100%;">

							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>

	<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
	<script src="{{asset('js/show-on-scroll.js')}}"></script>
	<script src="{{asset('js/app.js')}}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

	<script>
		flatpickr('#date_of_reservation', {
			dateFormat: 'd.m.Y',
			allowInput: false,
			disable: []
		});

		flatpickr('#time_of_reservation', {
			enableTime: true,
			time_24hr: true,
			noCalendar: true,
			minTime: "08:00",
			maxTime: "17:00"
		})

		// flatpickr('selector', options);
		// flatpickr('#flatpickr',{
		//   // A string of characters which are used to define how the date will be displayed in the input box.
		//   dateFormat: 'Y-m-d',
		//   // A reference to another input element.
		//   // This can be useful if you want to show the user a readable date, but return something totally different to the server.
		//   altFormat: "F j, Y",
		//   // Exactly the same as date format, but for the altInput field
		//   altInput: false,
		//   // This class will be added to the input element created by the altInput option. 
		//   // Note that altInput already inherits classes from the original input.
		//   altInputClass: "",
		//   // Allows the user to enter a date directly input the input field. By default, direct entry is disabled.
		//   allowInput: false,
		//   // Instead of body, appends the calendar to the specified node instead.
		//   appendTo: null,
		//   // Defines how the date will be formatted in the aria-label for calendar days, using the same tokens as dateFormat.
		//   // If you change this, you should choose a value that will make sense if a screen reader reads it out loud.
		//   ariaDateFormat: "F j, Y",
		//   // Whether clicking on the input should open the picker. You could disable this if you wish to open the calendar manually with.open()
		//   clickOpens: true,
		//   // Sets the initial selected date(s).
		//   // If you're using mode: "multiple" or a range calendar supply an Array of Date objects or an Array of date strings which follow your dateFormat.
		//   // Otherwise, you can supply a single Date object or a date string.
		//   defaultDate: null, 
		//   // Initial value of the hour element.
		//   defaultHour: 12, 
		//   // Initial value of the minute element. 
		//   defaultMinute: 0,
		//   // The minimum date that a user can start picking from, as a JavaScript Date.
		//   minDate: null,
		//   // The maximum date that a user can pick to, as a JavaScript Date.
		//   maxDate: null,
		//   // Dates to disable, using intervals
		//   // disable: [ { 'from': '2015-09-02', 'to': '2015-10-02' } ]
		//   disable: null,
		//   // Set disableMobile to true to always use the non-native picker.
		//   // By default, flatpickr utilizes native datetime widgets unless certain options (e.g. disable) are used.
		//   disableMobile: false,
		//   // See Enabling dates
		//   enabl: [],
		//   // Enables time picker
		//   enableTime: false,
		//   // Enables seconds in the time picker.
		//   enableSeconds: false,
		//   // Allows using a custom date formatting function instead of the built-in handling for date formats using dateFormat, altFormat, etc.
		//   formatDate: null, 
		//   // Adjusts the step for the hour input (incl. scrolling)
		//   hourIncrement: 1,
		//   // Displays the calendar inline
		//   inline: false,
		//   // Show the month using the shorthand version.
		//   shorthandCurrentMonth: false,
		//   // Adjusts the step for the minute input (incl. scrolling)
		//   minuteIncrement: 5,
		//   // "single"  "single", "multiple", or "range"
		//   mode: "single",
		//   // next/prev arrows
		//   prevArrow: '&lt;',
		//   nextArrow: '&gt;',
		//   // Function that expects a date string and must return a Date object
		//   parseDate: false,
		//   // Show the month using the shorthand version (ie, Sep instead of September).
		//   shorthandCurrentMonth: false,
		//   // Position the calendar inside the wrapper and next to the input element. (Leave false unless you know what you're doing.
		//   static: false,
		//   // Displays time picker in 24 hour mode without AM/PM selection when enabled.
		//   time_24hr: false,
		//   // Enables display of week numbers in calendar.
		//   weekNumbers: false,
		//   // Hides the day selection in calendar.
		//   // Use it along with enableTime to create a time picker.
		//   noCalendar: false 
		// });

		// callbacks
		// flatpickr('#flatpickr', {
		// 	// Function(s) to trigger on every date selection.
		// 	//   onChange: null 
		// 	// Function(s) to trigger on every time the calendar is closed.
		// 	//   onClose: null 
		// 	// Function(s) to trigger on every time the calendar is opened.
		// 	//   onOpen: null 
		// 	// Function to trigger when the calendar is ready.
		// 	//   onReady: null 
		// });
	</script>
</body>

</html>