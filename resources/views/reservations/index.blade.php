@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12" style="overflow: auto">
            <table class="table">
                <thead>
                    <th style="width:15%;">
                        Vrsta tretmana
                    </th>
                    <th style="width:15%;">
                        Datum rezervacije
                    </th>
                    <th style="width:30%;">
                        Vrijeme rezervacije
                    </th>
                    <th style="width:20%;">
                        Korisnik
                    </th>
                </thead>
                <tbody>
                    @foreach ($reservations as $reservation)
                    <tr>
                        <td>{{ $reservation-> reservation_type }}</td>
                        <td>{{ $reservation-> date_of_reservation }}</td>
                        <td>{{ $reservation-> time_of_reservation }}</td>
                        <td>{{ $reservation-> user_id }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection